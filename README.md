# PartyLine

>Projet officiel du Saclay Working Group <img src="https://i.imgur.com/Vs3YPLI.png" height=50>


PartyLine est le standard conçu et utilisé par les membres du SWG pour la diffusion des évènements de la vie étudiante se déroulant sur le Plateau de Saclay.

## Collaborateurs

* Binet Réseau — Polytechnique
* DaTA — ENSTA ParisTech
* Rezel — Télécom ParisTech

## MkDocs

Pour effectuer un rendu local de la documentation, veuillez executer 
```bash
$ pip install --user mkdocs-material pygments
```
