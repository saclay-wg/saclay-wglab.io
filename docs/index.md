# Page d'accueil

<img src=assets/images/logo.png width=300 align=right>

PartyLine est le standard conçu et utilisé par les membres du SWG pour la diffusion des évènements de la vie étudiante se déroulant sur le Plateau de Saclay.

Les contributeurs de cette spécification sont

* Binet Réseau — École Polytechnique
* DaTA — ENSTA ParisTech
* Rezel — Télécom ParisTech

Vous pouvez consulter la [spécification JSON](spec.md) pour plus d'informations.

## Évènements à venir

* 20/21 juillet: Session dev et discussions.

## Évènements passés

* 22/23 juin: Session dev pour progresser sur la spec et ses implémentations
* 29/30 juin: Session devᵇⁱˢ pour faire sécher les plâtres sur les progrès de la semaine d'avant
