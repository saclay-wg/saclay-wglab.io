# Spécification

Au travers de cette spécification, voici les usages en vigueur pour certains formats :

* Les dates sont formattée selon ISO8601, et plus précisément selon la façon dont JavaScript encode en JSON son format Date.  
  Format: "YYYY-MM-DDTHH:mm:ss.sssZ"  
  Exemple: "2019-03-17T01:43:18.338Z";
* Les UUID version 4 (aléatoires) sont utilisés;
* Les ressources distantes sont exprimées comme URL.

⚠️ Une version OpenAPI (et plus à jour) de cette documentation est disponible [sur swaggerhub](https://app.swaggerhub.com/apis/saclaywg/partyline-api/0.1.3#/).

## Modèles

### Modèle User

Ce modèle comporte quatre champs :

* `displayName` (String) : Le nom complet de la personne
* `email` (String) : Son adresse email d'école
* `photo` (URL) : Une URL vers son image de profil sur le système de son école
* `uuid` (UUID v4) : Son UUID sur le système de son école

#### Exemple

```JSON
{
    "displayName": "Raúl Rahmani",
    "email": "r.rahmani@example.com",
    "photo": "https://assets.example.com/photo/e06be8bf-cf83-4735-a8f2-6d6e02e5b120",
    "uuid": "e06be8bf-cf83-4735-a8f2-6d6e02e5b120",
}
```

### Modèle Event

Ce modèle comporte les champs suivants :

* `nodeId` (String): Le système qui stocke l'event, et donc l'auteur de l'event
* `author` (Objet) : Référence l'auteur de l'event
    - `id` (String) : "Clé primaire" qui servira à récupérer les informations de l'auteur⋅ice dans le système de son école (qui est référencée par le champ `nodeId`)
    - `type` (String) : Type de l'auteur. Deux valeurs possibles : `User` ou `Group`
* `description` (String) : Une description en paragraphe de l'évènement. Peut contenir des sauts de ligne
* `title` (String) : Titre canonique de l'évènement
* `place` (String) : La localisation. Devrait pouvoir être entrée dans un service de cartographie en ligne (type Google Maps) pour faciliter la venue des invités
* `startTime` (Timestamp) : La timestamp de début de l'évènement. Formattée selon la norme exprimée ci-dessus.
* `endTime` (Timestamp) : La timestamp de fin de l'évènement. Formattée selon la norme exprimée ci-dessus.
* `uuid` (UUID) : l'UUID de l'évènement sur le système de l'école qui l'a produit.

#### Exemple

```JSON
{
    "nodeOwnerId": "telecom-paristech",
    "author": {
        "id": "8628c754-ee94-464e-9e95-ef47296ef5db",
        "type": "user",
    },
    "description": "Lorem ipsum\nBlablabla…",
    "endTime": "2019-03-17T01:08:58.109Z",
    "place": "Campus",
    "startTime": "2019-03-16T23:08:58.109Z",
    "title": "Soirée ramassage de déchets",
    "uuid": "27740b47-0507-40de-b215-5dfa6942f6d8",
}
```

### Modèle Post

Le reste des champs est :

* `nodeId` (String): Le système qui stocke le post. Ce post répond à un post provenant de cet ID
* `parent` (Object) : Référence le parent du post. Peut ne pas être précisé si le post n'a pas de parent, ie qu'il n'est pas un commentaire.
    - `id` (String) : Potentiel parent du post. Ce champs peut contenir un UUID vers le parent, qu'il soit de type `Post` ou `Event`.
    - `type` (String) : Précise le type du parent. Deux valeurs possibles : `Post` ou `Event`.
* `title` (String) : Titre canonique de l'article
* `description` (String) : Contenu de l'article
* `author` (Object) : référence l'auteur du message
    - `id` (String) : "Clé primaire" qui servira à récupérer les informations de l'auteur⋅ice dans le système de son école 
    - `nodeId` (String) : École référençant l'auteur du commentaire (qui n'est pas forcément celle qui stocke le commentaire, qui lui est stocké la où son parent est stocké)
    - `type` (String) : Type de l'auteur. Deux valeurs possibles : `User` ou `Group`
* `createdAt` (Timestamp) : Timestamp de publication de l'article, formattée selon la norme ci-dessus
* `updatedAt` (Timestamp) : Timestamp de la plus récente édition de l'article, formattée selon la norme ci-dessus
* `attachements` ([URL]) : Une liste d'URL de ressources distances qui seront mises à disposition avec l'article (photos, documents…)
* `uuid` (UUID) :UUID dans le système qui héberge l'article

#### Exemple

```JSON
{
    "nodeId": "telecom-paristech",
    "attachments": [],
    "parent": {
        "id": "f88f228f-5b7b-4bef-8e49-efc60f811d49",
        "type": "event",
    },
    "author": {
        "id": "e06be8bf-cf83-4735-a8f2-6d6e02e5b120",
        "type": "group",
        "nodeId": "polytechnique",
    },
    "createdAt": "2019-03-16T23:08:58.109Z",
    "description": "Nous irons tous⋅tes ramasser les déchets traînant sur nos campus respectifs !",
    "title": "Soirée ramassage de déchets",
    "updatedAt": "2019-03-16T23:08:58.109Z",
    "uuid": "5b6cbbe5-2fca-434c-9ffa-754fddd081e2",
}
```

### Modèle Group

Ce modèle comporte quatre champs, et sert à représenter un regroupement de personnes physiques ou morales au sein d'une écoles, ou du campus

* `displayName` (String) : Le nom d'affichage de ce groupe
* `photo` (URL) : Une URL vers son image de profil sur le système de son école 
* `email` (String) : Son adresse email 
* `uuid` (UUID v4) : Son UUID sur le système de son école

#### Exemple

```JSON
{
    "photo": "https://assets.example/photo/4d7e1c58-98e7-4c02-aeac-d1c0407d7cb9",
    "email": "spam@rezel.net",
    "uuid": "4d7e1c58-98e7-4c02-aeac-d1c0407d7cb9",
    "displayName": "Rezel"
}
```
## Routes

Cette partie décrit les différentes routes de l'API. 
De manière générale, tous les POST servent à la propagation des évènements créés ou modifiés.

##### Erreurs

La transmission des erreurs se fait par code HTTP standards: 403 pour unauthorized, 404 pour objet non trouvé etc.
